package com.practise;

public class MoveThread extends Thread {

    CircularBuffer<String> firstBuffer;
    CircularBuffer<String> secondBuffer;
    int threadId;
    int count;

    public MoveThread(CircularBuffer<String> firstBuffer,
                           CircularBuffer<String> secondBuffer,
                           int threadId,
                           int count) {
        super();
        this.firstBuffer = firstBuffer;
        this.secondBuffer = secondBuffer;
        this.threadId = threadId;
        this.count = count;
    }

    public void run() {
        for (int i = 0; i < count; i++) {
            String message = firstBuffer.get();
            secondBuffer.add("Thread #" + threadId + " move message: " + message);
        }
    }
}
