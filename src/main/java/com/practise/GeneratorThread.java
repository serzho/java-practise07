package com.practise;

import java.util.*;

public class GeneratorThread extends Thread {

    CircularBuffer<String> buffer;
    int threadId;
    int count;

    public GeneratorThread(CircularBuffer<String> buffer, int threadId, int count) {
        super();
        this.buffer = buffer;
        this.threadId = threadId;
        this.count = count;
    }

    public void run() {
        for (int i = 0; i < count; i++) {
            buffer.add("Thread #" + threadId + " generate message: " + new Date().toString());
        }
    }
}
