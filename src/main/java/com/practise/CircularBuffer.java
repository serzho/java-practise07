package com.practise;
import java.util.*;
public class CircularBuffer<T> {
    
    protected int size;
    protected int beginPointer;
    protected int endPointer;
    protected boolean isFull;
    protected boolean isEmpty;
    protected List<T> elements;
    public CircularBuffer(int size) {
        this.size = size;
        this.beginPointer = 0;
        this.endPointer = 0;
        this.isFull = false;
        this.isEmpty = true;
        
        this.initElements();
    }
    public synchronized void add(T element) {
        if (!isFull) { 
            addElement(element);
            this.notifyAll();
        } else {
            try {
                while (!isFull)
                    this.wait();
                addElement(element);
            } catch (InterruptedException exp) {
                exp.printStackTrace();
            }
        }
    }

    protected synchronized void addElement(T element) {
        elements.set(endPointer, element);
        increaseEndPointer();
        isEmpty = false;
    }

    protected synchronized void increaseEndPointer() {
        if (endPointer + 1 >= size) {
            endPointer = 0;
        } else
            endPointer++;
        if (endPointer == beginPointer) {
            isFull = true;
        }
    }
    public synchronized T get() {
        T result = null;
        if (!isEmpty) {
            result = getElement();
            this.notifyAll();
        } else {
            try {
                while(!isEmpty)
                    this.wait();
                result = getElement();
            } catch (InterruptedException exp) {
                exp.printStackTrace();
            }
        }
        return result;
    }

    protected synchronized T getElement() {
        T result =  elements.get(beginPointer);
        increaseBeginPointer();
        isFull = false;
        return result;
    }
    protected synchronized void increaseBeginPointer() {
        if (beginPointer + 1 >= size)
            beginPointer = 0;
        else
            beginPointer++;
        if (beginPointer == endPointer)
            isEmpty = true;
    }
    private void initElements() {
        this.elements = new ArrayList(this.size);
        for (int i = 0; i < this.size; i++)
            this.elements.add(null);
    }
}
