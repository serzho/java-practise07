package com.practise;
import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;
public class IntegrationTest {

    @Test
    public void test() {
        CircularBuffer<String> firstBuffer = new CircularBuffer(30);
        CircularBuffer<String> secondBuffer = new CircularBuffer(30);
        int count = 1000;

        for (int i = 0; i < 5; i++) {
            new GeneratorThread(firstBuffer, i, count).start();
        }

        for (int i = 0; i < 2; i++) {
            new MoveThread(firstBuffer, secondBuffer, i, count).start();
        }

        for (int i = 0; i < count; i++) {
            System.out.println(secondBuffer.get());
        }
    }
}
