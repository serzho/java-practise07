package com.practise;
import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;
public class CircularBufferTest {
    CircularBuffer<Integer> buffer;
    @Before
    public void up() {
        buffer = new CircularBuffer(3);
    }
    @Test
    public void should_add_elements() {
        buffer.add(1);
        buffer.add(2);
        assertThat(buffer.elements, hasItem(1));
        assertThat(buffer.elements, hasItem(2));
        assertThat(buffer.elements, not(hasItem(3)));
    }
    @Test
    public void test_on_adding_element_end_pointer_should_increase_by_one() {
        buffer.add(1);
        assertThat(buffer.endPointer, is(1));
    }
    @Test
    public void should_loop_begin_pointer_on_increase() {
        
        buffer.beginPointer = 0;
        buffer.endPointer = 1;
        buffer.increaseEndPointer();
        assertThat(buffer.endPointer, is(2));
        buffer.beginPointer = 1;
        buffer.endPointer = 2;
        buffer.increaseEndPointer();
        assertThat(buffer.endPointer, is(0));
    }
    @Test
    public void signed_as_full() {
        buffer.beginPointer = 2;
        buffer.endPointer = 1;
        buffer.add(1);
        assertThat(buffer.isFull, is(true));
    }
    @Test
    public void should_increase_end_pointer_on_boundaries() {
    
        buffer.beginPointer = 1;
        buffer.endPointer = 2;
        buffer.increaseEndPointer();
        assertThat(buffer.endPointer, is(0));
    }
    @Test
    public void should_get_elements() {
        buffer.add(1);
        buffer.add(2);
        int element = (Integer) buffer.get();
        assertThat(element, is(equalTo(1)));
        element = (Integer) buffer.get();
        assertThat(element, is(equalTo(2)));
    }
    @Test
    public void should_be_signed_as_empty() {
        buffer.add(1);
        buffer.get();
        assertThat(buffer.isEmpty, is(true));
    }
}
